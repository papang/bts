package com.services.checklistitem;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.models.ChecklistItem;
import com.dao.repositories.ChecklistItemRepository;

@Service
public class ChecklistItemService {

	@Autowired
	private ChecklistItemRepository checklistItemRepository;

	public List<ChecklistItem> getAllProfile() {
		return checklistItemRepository.findAll();
	}

	public Optional<ChecklistItem> getProfileById(Long id) {
		return checklistItemRepository.findById(id);
	}

	public ChecklistItem createProfile(ChecklistItem Profile) {
		return checklistItemRepository.save(Profile);
	}

	public ChecklistItem updateProfile(Long id, ChecklistItem profile) {
		Optional<ChecklistItem> existingProfile = checklistItemRepository.findById(id);
		if (existingProfile.isPresent()) {
			ChecklistItem updatedProfile = existingProfile.get();
			updatedProfile.setName(profile.getName());
			updatedProfile.setStatus(profile.getStatus());
			return checklistItemRepository.save(updatedProfile);
		}
		return null;
	}

	public void deleteProfile(Long id) {
		checklistItemRepository.deleteById(id);
	}

}
