package com.services.checklistitem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.dao.models.ChecklistItem;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/checklistitem")
public class ChecklistItemController {

	@Autowired
	private ChecklistItemService checklistItemService;
	@Autowired

	@GetMapping("")
	public List<ChecklistItem> getAllProfile() {
		return checklistItemService.getAllProfile();
	}

	@GetMapping("/{id}")
	public Optional<ChecklistItem> getProfileById(@PathVariable Long id) {
		return checklistItemService.getProfileById(id);
	}

	@PostMapping
	public ChecklistItem createProfile(@RequestBody ChecklistItem profile) {
		return checklistItemService.createProfile(profile);
	}

	@PutMapping("/{id}")
	public ChecklistItem updateProfile(@PathVariable Long id, @RequestBody ChecklistItem profile) {
		return checklistItemService.updateProfile(id, profile);
	}

	@DeleteMapping("/{id}")
	public void deleteProfile(@PathVariable Long id) {
		checklistItemService.deleteProfile(id);
	}

}
