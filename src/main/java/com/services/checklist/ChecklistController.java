package com.services.checklist;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.dao.models.Checklist;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/checklist")
public class ChecklistController {

	@Autowired
	private ChecklistService checklistService;
	@Autowired

	@GetMapping("")
	public List<Checklist> getAllChecklist() {
		return checklistService.getAllChecklist();
	}

	@GetMapping("/{id}")
	public Optional<Checklist> getChecklistById(@PathVariable Long id) {
		return checklistService.getChecklistById(id);
	}

	@PostMapping
	public Checklist createChecklist(@RequestBody Checklist Checklist) {
		return checklistService.createChecklist(Checklist);
	}

	@PutMapping("/{id}")
	public Checklist updateChecklist(@PathVariable Long id, @RequestBody Checklist Checklist) {
		return checklistService.updateChecklist(id, Checklist);
	}

	@DeleteMapping("/{id}")
	public void deleteChecklist(@PathVariable Long id) {
		checklistService.deleteChecklist(id);
	}

}
