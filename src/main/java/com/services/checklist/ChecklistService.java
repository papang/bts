package com.services.checklist;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.models.Checklist;
import com.dao.repositories.ChecklistRepository;

@Service
public class ChecklistService {

	@Autowired
	private ChecklistRepository checklistRepository;

	public List<Checklist> getAllChecklist() {
		return checklistRepository.findAll();
	}

	public Optional<Checklist> getChecklistById(Long id) {
		return checklistRepository.findById(id);
	}

	public Checklist createChecklist(Checklist Checklist) {
		return checklistRepository.save(Checklist);
	}

	public Checklist updateChecklist(Long id, Checklist Checklist) {
		Optional<Checklist> existingChecklist = checklistRepository.findById(id);
		if (existingChecklist.isPresent()) {
			Checklist updatedChecklist = existingChecklist.get();
			updatedChecklist.setName(Checklist.getName());
			return checklistRepository.save(updatedChecklist);
		}
		return null;
	}

	public void deleteChecklist(Long id) {
		checklistRepository.deleteById(id);
	}

}
