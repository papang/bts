package com.dao.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "checklist")
@Setter
@Getter
public class Checklist {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long checklistId;

    private String name;
}
