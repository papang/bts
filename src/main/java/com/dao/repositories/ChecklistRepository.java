package com.dao.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dao.models.Checklist;

@Repository
public interface ChecklistRepository extends JpaRepository<Checklist, Long> {
	
	
}